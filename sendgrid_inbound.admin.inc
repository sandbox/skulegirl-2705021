<?php
/**
 * @file
 * Administrative functions.
 */

/**
 * Menu callback for admin settings.
 */
function sendgrid_inbound_settings_form() {
  $form = array();

  $form['sendgrid_inbound_email_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Email domain'),
    '#default_value' => variable_get('sendgrid_inbound_email_domain', 'example.co.uk'),
  );

  return system_settings_form($form);
}
