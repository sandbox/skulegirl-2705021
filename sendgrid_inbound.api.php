<?php

/**
 * @file
 * Describe hooks provided by the Sendgrid Inbound module.
 */

/**
 * Alter argument that sends to Rules.
 *
 * @see sendgrid_inbound_callback()
 */
function hook_sendgrid_inbound_alter(&$args) {
  $args['text']['value'] = t('hello');
}
