-- SUMMARY --

This module provides integration with Sendgrid inbound parse email processing.

-- FEATURES --

The main module provides 'Incoming message is received' event for Rules module 
with related variables.

Also it has integration with some modules:
  * Privatemsg - Users will be able reply to private messages by email.

-- REQUIREMENTS --

* Rules

-- INSTALLATION --

1. Enable 'Sendgrid Inbound' module.
2. Go to 'admin/config/services/sendgrid/inbound' page and add email domain. 
 (e.g. in.example.com)
3. Go to https://app.sendgrid.com/settings/parse and add new inbound host 
 (e.g. in.example.com) and webhook URL of your domain + 
 /sendgrid/webhook/inbound, 
 e.g.  http://www.example.com/sendgrid/webhook/inbound

 NOTE: It is important that your webhook URL be the exact url that your 
 website will locate the hook at, i.e. if you redirect http://example.com to
 https://www.example.com, then you need to use the latter, otherwise your
 calls will fail with a 301 permanently moved response code.

-- FEATURES --
There is a hook_sendgrid_inbound_alter API which allows you to change the 
message before rules are triggered. sendgrid_inbound itself implements this 
and strips out all of the quoted text in the message.
